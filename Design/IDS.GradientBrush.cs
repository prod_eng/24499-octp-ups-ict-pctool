// IDS.GradientBrush.cs
// Version 1.2
//
// History
// 1.0 -- first release
// 1.1 -- better default colors
// 1.2 -- bug fix

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace IDS
{
    // GradientBrushTypeConverter provides expandable Property node interface functionality to GCColorOffset properties.
    public class GradientBrushTypeConverter : System.ComponentModel.TypeConverter
    {
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            return TypeDescriptor.GetProperties(typeof(GradientBrush));
        }
    }

    [TypeConverter(typeof(GradientBrushTypeConverter)), Serializable, Browsable(true)]
    public class GradientBrush : IDisposable
    {
        // gradient modes supported
        public enum GradientMode
        {
            None,
            Horizontal,
            Vertical,
            ForwardDiagonal,
            BackwardDiagonal,
            Angle,
        }

        // only necessary if we want to invalidate the parent on control changes
        System.Windows.Forms.Control Parent = null;

        public GradientBrush() { }
        public GradientBrush(System.Windows.Forms.Control control) { Parent = control; Rectangle = Parent.ClientRectangle; }
        public GradientBrush(Color c) { Color1 = c; }
        public GradientBrush(Color c1, Color c2, Rectangle r, GradientMode mode)
        {
            Color1 = c1;
            Color2 = c2;
            Rectangle = r;
            Mode = mode;
        }
        public GradientBrush(Color c1, Color c2, Rectangle r, float angle)
        {
            Color1 = c1;
            Color2 = c2;
            Rectangle = r;
            Mode = GradientMode.Angle;
            Angle = angle;
        }
       
        GradientMode mMode = GradientMode.ForwardDiagonal;
        [DefaultValue(GradientMode.None), Category("Appearance"), Description("Type of gradient to apply to brush."), Browsable(true)]
        public GradientMode Mode
        {
            get { return mMode; }
            set { if (mMode != value) { mMode = value; Dispose(); } }
        }

        Color mColor1 = Color.White;
        [Category("Appearance"), Description("First color for the brush."), Browsable(true)]
        public Color Color1
        {
            get { return mColor1; }
            set { if (mColor1 != value) { mColor1 = value; Dispose(); } }
        }

        Color mColor2 = Color.Gainsboro;
        [Category("Appearance"), Description("Second color for the brush."), Browsable(true)]
        public Color Color2
        {
            get { return mColor2; }
            set { if (mColor2 != value) { mColor2 = value; if (mMode != GradientMode.None) Dispose(); } }
        }

        float mAngle = 0;
        [DefaultValue(0), Category("Appearance"), Description("Angle to apply the gradient."), Browsable(true)]
        public float Angle
        {
            get { return mAngle; }
            set { if (mAngle != value) { mAngle = value; if (mMode == GradientMode.Angle) Dispose(); } }
        }

        Rectangle mRectangle = new Rectangle(0, 0, 100, 100);
        [Category("Appearance"), Description("Size of the gradient."), Browsable(true)]
        public Rectangle Rectangle
        {
            get { return mRectangle; }
            set
            {
                if (value.Width <= 0) value.Width = 1;
                if (value.Height <= 0) value.Height = 1;
                if (mRectangle != value)
                {
                    mRectangle = value;
                    if (mMode != GradientMode.None)
                        Dispose();
                }
            }
        }

        Brush mBrush = null;
        public static implicit operator Brush(GradientBrush b)
        {
            if (b.mBrush == null)
                b.Create();
            return b.mBrush;
        }

        ~GradientBrush()
        {
            Dispose();
        }

        void Create()
        {
            switch (mMode)
            {
                default:
                case GradientMode.None:
                    break;
                case GradientMode.Horizontal:
                    mBrush = new LinearGradientBrush(mRectangle, mColor1, mColor2, LinearGradientMode.Horizontal);
                    return; 
                case GradientMode.Vertical:
                    mBrush = new LinearGradientBrush(mRectangle, mColor1, mColor2, LinearGradientMode.Vertical);
                    return;
                case GradientMode.ForwardDiagonal:
                    mBrush = new LinearGradientBrush(mRectangle, mColor1, mColor2, LinearGradientMode.ForwardDiagonal);
                    return;
                case GradientMode.BackwardDiagonal:
                    mBrush = new LinearGradientBrush(mRectangle, mColor1, mColor2, LinearGradientMode.BackwardDiagonal);
                    return;
                case GradientMode.Angle:
                    mBrush = new LinearGradientBrush(mRectangle, mColor1, mColor2, mAngle);
                    return;
            }
            mBrush = new SolidBrush(mColor1);
        }

        public void Dispose()
        {
            if (mBrush != null)
            {
                mBrush.Dispose();
                mBrush = null;
            }
            if (Parent != null)
                Parent.Invalidate();
        }
    }
}
