// IDS.Form.cs
// Version 1.1
//
// History
// 1.0 -- first release
// 1.1 -- reduced flicker on repaint
//
//
// This class extends the base System.Windows.Forms.Form class
// - provides gradient options for form background
// - provides easy access to system menu

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace IDS
{
    public class Form : System.Windows.Forms.Form
    {
        IDS.GradientBrush mBackgroundBrush = null;
        SysMenu mSystemMenu = null;

        // hide the back color
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new Color BackColor
        {
            get { return mBackgroundBrush.Color1; }
            set { }
        }

        // display properties for background brush
        [Category("Appearance"), Description("Background brush properties."), Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public IDS.GradientBrush BackgroundBrush
        {
            get { return mBackgroundBrush; }
        }

        public Form()
        {
            mSystemMenu = new SysMenu(this);
            mBackgroundBrush = new IDS.GradientBrush(this);
        }
       
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            mBackgroundBrush.Rectangle = ClientRectangle;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(mBackgroundBrush, ClientRectangle);
        }

        // hook into the system menu
        protected override void WndProc (ref Message msg )
        {
            const int WM_SYSCOMMAND = 0x0112;
            if (msg.Msg == WM_SYSCOMMAND)
            {
                SysMenu.Handler callback = mSystemMenu.GetCallback(msg.WParam.ToInt32());
                if (callback != null)
                    callback();
            }
            base.WndProc(ref msg);
        }

        // get system menu
        public SysMenu SystemMenu
        {
            get
            {
                if (mSystemMenu.Valid)
                    return mSystemMenu;
                return null;
            }
        }

        // A class that helps to manipulate the system menu of a passed form.
        public class SysMenu
        {
            public delegate void Handler();

            class Win32
            {
                [DllImport("USER32", EntryPoint = "GetSystemMenu", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
                static public extern IntPtr GetSystemMenu(IntPtr WindowHandle, int bReset);
                [DllImport("USER32", EntryPoint = "AppendMenuW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
                static public extern int AppendMenu(IntPtr MenuHandle, int Flags, int NewID, String Item);
                [DllImport("USER32", EntryPoint = "InsertMenuW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
                static public extern int InsertMenu(IntPtr hMenu, int Position, int Flags, int NewId, String Item);
            }
            Form Form = null;
            IntPtr m_SysMenu = IntPtr.Zero;
            System.Collections.ArrayList Callback = new System.Collections.ArrayList();

            // Values taken from MSDN.
            [Flags]
            public enum ItemFlags : int
            {
                Unchecked = 0x00000000,    // ... is not checked
                String = 0x00000000,    // ... contains a string as label
                Disabled = 0x00000002,    // ... is disabled
                Grayed = 0x00000001,    // ... is grayed
                Checked = 0x00000008,    // ... is checked
                Popup = 0x00000010,    // ... Is a popup menu. Pass the
                //     menu handle of the popup
                //     menu into the ID parameter.
                BarBreak = 0x00000020,    // ... is a bar break
                Break = 0x00000040,    // ... is a break
                ByPosition = 0x00000400,    // ... is identified by the position
                ByCommand = 0x00000000,    // ... is identified by its ID
                Separator = 0x00000800     // ... is a seperator (String and
                //     ID parameters are ignored).
            }
            
            // hide the constuctor to the outside world
            internal SysMenu(Form form)
            {
                Form = form;
            }

            internal bool Valid
            {
                get
                {
                    if (m_SysMenu == IntPtr.Zero)
                        m_SysMenu = Win32.GetSystemMenu(Form.Handle, 0);
                    return m_SysMenu != IntPtr.Zero;
                }
            }

            int AddCallback(Handler callback)
            {
                Callback.Add(callback);
                return 0xFF + Callback.Count;
            }

            internal Handler GetCallback(int id)
            {
                id -= 0x100;
                if (id >= 0 && id < Callback.Count)
                    return (Handler)Callback[id];
                return null;
            }
               
            // Insert a separator at the given position index starting at zero.
            public bool InsertSeparator(int pos)
            {
                return Win32.InsertMenu(
                    m_SysMenu,
                    pos,
                    (int)(ItemFlags.Separator | ItemFlags.ByPosition),
                    0,
                    "") == 0;
            }

            // Simplified InsertMenu(), that assumes that Pos is a relative
            // position index starting at zero
            public bool InsertMenu(int pos, String item, Handler callback)
            {
                return InsertMenu(pos, item, callback, ItemFlags.ByPosition | ItemFlags.String);
            }

            // Insert a menu at the given position. The value of the position
            // depends on the value of Flags. See the article for a detailed
            // description.
            public bool InsertMenu(int pos, String item, Handler callback, ItemFlags flags)
            {
                return Win32.InsertMenu(m_SysMenu, pos, (Int32)flags, AddCallback(callback), item) == 0;
            }

            // Appends a seperator
            public bool AppendSeparator()
            {
                return Win32.AppendMenu(m_SysMenu, (int)ItemFlags.Separator, 0, "") == 0;
            }

            // This uses the ItemFlags.String as default value
            public bool AppendMenu(String item, Handler callback)
            {
                return AppendMenu(item, callback, ItemFlags.String);
            }
            // Superseded function.
            public bool AppendMenu(String item, Handler callback, ItemFlags flags)
            {
                return Win32.AppendMenu(m_SysMenu, (int)flags, AddCallback(callback), item) == 0;
            }

            // Reset's the window menu to it's default
            public static void Reset(Form Frm)
            {
                Win32.GetSystemMenu(Frm.Handle, 1);
            }

            // Checks if an ID for a new system menu item is OK or not
            static bool VerifyItemID(int ID)
            {
                return (bool)(ID < 0xF000 && ID > 0);
            }
        }
    }
}
