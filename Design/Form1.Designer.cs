namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            gSM  = new StateMachineClass();

            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.PCBANumberLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.PortStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.DUTStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.HistoryBox = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AssemblyNumberLabel = new System.Windows.Forms.Label();
            this.FaceLabel = new IDS.RotatedLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PCBANumberLabel
            // 
            this.PCBANumberLabel.AutoSize = true;
            this.PCBANumberLabel.BackColor = System.Drawing.Color.Transparent;
            this.PCBANumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PCBANumberLabel.Location = new System.Drawing.Point(6, 12);
            this.PCBANumberLabel.Name = "PCBANumberLabel";
            this.PCBANumberLabel.Size = new System.Drawing.Size(194, 31);
            this.PCBANumberLabel.TabIndex = 1;
            this.PCBANumberLabel.Text = "PCBA number:";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel.Location = new System.Drawing.Point(112, 87);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(93, 31);
            this.StatusLabel.TabIndex = 3;
            this.StatusLabel.Text = "Ready";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 31);
            this.label1.TabIndex = 4;
            this.label1.Text = "Status:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PortStatusLabel,
            this.DUTStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 382);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(715, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // PortStatusLabel
            // 
            this.PortStatusLabel.AutoSize = false;
            this.PortStatusLabel.Name = "PortStatusLabel";
            this.PortStatusLabel.Size = new System.Drawing.Size(200, 17);
            this.PortStatusLabel.Text = "PortStatusLabel";
            // 
            // DUTStatusLabel
            // 
            this.DUTStatusLabel.Name = "DUTStatusLabel";
            this.DUTStatusLabel.Size = new System.Drawing.Size(90, 17);
            this.DUTStatusLabel.Text = "DUTStatusLabel";
            // 
            // HistoryBox
            // 
            this.HistoryBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HistoryBox.ContextMenuStrip = this.contextMenuStrip1;
            this.HistoryBox.FormattingEnabled = true;
            this.HistoryBox.Location = new System.Drawing.Point(12, 229);
            this.HistoryBox.Name = "HistoryBox";
            this.HistoryBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.HistoryBox.Size = new System.Drawing.Size(691, 134);
            this.HistoryBox.TabIndex = 6;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // AssemblyNumberLabel
            // 
            this.AssemblyNumberLabel.AutoSize = true;
            this.AssemblyNumberLabel.BackColor = System.Drawing.Color.Transparent;
            this.AssemblyNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssemblyNumberLabel.Location = new System.Drawing.Point(6, 43);
            this.AssemblyNumberLabel.Name = "AssemblyNumberLabel";
            this.AssemblyNumberLabel.Size = new System.Drawing.Size(238, 31);
            this.AssemblyNumberLabel.TabIndex = 8;
            this.AssemblyNumberLabel.Text = "Assembly number:";
            // 
            // FaceLabel
            // 
            this.FaceLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FaceLabel.AutoSize = true;
            this.FaceLabel.BackColor = System.Drawing.Color.Transparent;
            this.FaceLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FaceLabel.Font = new System.Drawing.Font("Wingdings", 78F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.FaceLabel.ForeColor = System.Drawing.Color.Green;
            this.FaceLabel.Location = new System.Drawing.Point(566, 9);
            this.FaceLabel.Name = "FaceLabel";
            this.FaceLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FaceLabel.RotationAngle = 0D;
            this.FaceLabel.Size = new System.Drawing.Size(137, 115);
            this.FaceLabel.TabIndex = 9;
            this.FaceLabel.Text = "J";
            this.FaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FaceLabel.TextDirection = IDS.RotatedLabel.Direction.Clockwise;
            this.FaceLabel.TextOrientation = IDS.RotatedLabel.Orientation.Rotate;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(284, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 164);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(232, 20);
            this.textBox1.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 404);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.AssemblyNumberLabel);
            this.Controls.Add(this.HistoryBox);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.PCBANumberLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.FaceLabel);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label PCBANumberLabel;
        public System.Windows.Forms.Label StatusLabel;
        public System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel PortStatusLabel;
        private System.Windows.Forms.ListBox HistoryBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        //private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel DUTStatusLabel;
        private System.Windows.Forms.Label AssemblyNumberLabel;
        private IDS.RotatedLabel FaceLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
    }
}

