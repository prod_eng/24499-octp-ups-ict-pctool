﻿// IDS.TextLogIO.cs
// Version 4.7
// .NET 4.0+
//
// Required Assembly References: System.Data.DataSetExtensions
// 
// Provides easy IO operations for Master Pass, Sync, Universal Pack and Master PQL text logs.
// !!! NOT !!! intended for use with any other logs.

// History
// 4.7 -- Rolling back threaded write operations as they are not thread-safe.
// 4.6 -- Fixed GetPairedEntry() to account for Sync logs with extra info (more than two items per line).
// 4.5 -- Added support for returning the serial number synced to a supplied serial number.
// 4.4 -- Fixed unhandled exception in ArchivalIsNecessary when trying to read a log file that does not yet exist.
// 4.3 -- Fixed bug in PackLog.PQLPreviouslyPackaged when PQL was not found to have been previously packaged.
//        Cannot union an empty DataTable.
// 4.2 -- Increased formatting line length tolerance to 5 characters to accomodate unpadded 
//        DateTimes (3 areas: month, day, hour) on top of the existing 2 character leeway.
//
//        Accounted for possibility that log to be archived is not in root folder of tool.
//        Archival folder for given log will always be relative to the log's folder, not
//        necessarily the folder of the tool being run.
// 4.1 -- Added automatic archival functions. Optimized parallelization.
// 4.0 -- SyncLog logging is now possible for syncing more than 2 items
// 3.0 -- new methods added for use with new universal packaging tool
// 2.1 -- added single digit leeway to formatting check to account for logs
//        with both 5-digit and 6-digit pql numbers in them.
// 2.0 -- optional timeouts for write operations added
// 1.0 -- first release

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.Linq;

namespace IDS
{
    /// <summary>
    /// Provides easy IO operations for Master Pass, Sync, and Master PQL text logs.  NOT intended for use with any other logs.
    /// </summary>
    static public class TextLogIO
    {
        const int TIMEOUT_MS = 3000;
        static private int _line_count_threshold = 6000;
        static private int _line_count_crop = 5000;

        static public int ARCHIVE_LINE_COUNT_THRESHOLD
        {
            get { return _line_count_threshold; }
            set { _line_count_threshold = value; }
        }

        static public int ARCHIVE_LINE_COUNT_CROP
        {
            get { return _line_count_crop; }
            set { _line_count_crop = value; }
        }

        /// <summary>
        /// Provides easy IO operations with Master PQL text logs.  NOT intended for use with any other logs.
        /// </summary>
        static public class PQLLog
        {
            /// <summary>
            /// Used when looking up box labels for packaging. P = part. Q = quantity. L = lot.
            /// </summary>
            public class PQL
            {
                private string mPQL;
                private string mPart;
                private string mLot;
                private int mQTY;

                /// <summary>
                /// PQL number
                /// </summary>
                public string PQLNumber
                {
                    get { return mPQL; }
                    set { mPQL = value; }
                }

                /// <summary>
                /// Part number
                /// </summary>
                public string Part
                {
                    get { return mPart; }
                    set { mPart = value; }
                }

                /// <summary>
                /// Lot number
                /// </summary>
                public string Lot
                {
                    get { return mLot; }
                    set { mLot = value; }
                }

                /// <summary>
                /// Box quantity
                /// </summary>
                public int QTY
                {
                    get { return mQTY; }
                    set { mQTY = value; }
                }

                /// <summary>
                /// An empty PQL
                /// </summary>
                public PQL()
                {
                    this.PQLNumber = "";
                    this.Part = "";
                    this.Lot = "";
                    this.QTY = 0;
                }

                /// <summary>
                /// Checks whether or not the PQL container is considered empty (no PQL was found and loaded in to it)
                /// </summary>
                /// <returns>True if empty, otherwise false.</returns>
                public bool IsEmpty()
                {
                    return (this.PQLNumber == "" & this.Part == "" & this.Lot == "" & this.QTY == 0);
                }
            }

            /// <summary>
            /// Attempts to find the specified PQL number in the master PQL log and return the data for that PQL.
            /// </summary>
            /// <param name="pqlnumber">The PQL number to search for preceeded by the string 'PQL'.</param>
            /// <param name="pqllog">The full path of the Master PQL log.</param>
            /// <returns>Container with all PQL data (PQL Number, Part, Lot, Quantity).</returns>
            static public PQL FindPQL(string pqlnumber, string pqllog)
            {
                // Attempt to get a clean version of the log as a list of lines.
                //List<string> cleanfiletxt = CleanedFileText(pqllog, true);
                // Get a new empty PQL ready for return.
                PQL mPQL = new PQL();
                // Make sure the passed pql number to search for doesn't have any leading or trailing whitespace.
                pqlnumber = pqlnumber.Trim();
                // The values in each valid line will appear in this order 0 = PQL, 1 = Part, 2 = Lot, 3 = QTY
                int PQLNumber = 0;
                int Part = 1;
                int Lot = 2;
                int QTY = 3;
                string[] all_lines = File.ReadAllLines(pqllog);
                // Iterate the lines of the pql log.
                Parallel.For(0, all_lines.Length, (i, loopState) =>
                {
                    // Get separated values from the line.                    
                    var values = all_lines[i].Split('\t');
                    // If 4 and only 4 values were found, the line is considered valid and we continue...
                    if (values != null && values.Length == 4)
                    {
                        // If the PQL number on this line matches what we're looking for...
                        if (values[PQLNumber] == pqlnumber)
                        {
                            // Load the values in to the container we're returning.
                            mPQL.PQLNumber = values[PQLNumber];
                            mPQL.Part = values[Part];
                            mPQL.Lot = values[Lot];
                            int mQTY = 0;
                            Int32.TryParse(values[QTY], out mQTY);
                            mPQL.QTY = mQTY;
                            loopState.Break();
                        }
                    }
                });
                // Return either an 'empty' container or the values associated with the PQL we were searching for.
                return mPQL;
            }
        }

        /// <summary>
        /// Provides easy IO operations for the universal packaging tool.  MIGHT be applicable to other special case packaging tools 
        /// based upon the universal packaging tool.  (This class assumes pack logs will always have a clean format.)
        /// </summary>
        static public class PackLog
        {
            static DateTime LAST_ARCHIVAL = new DateTime();

            /// <summary>
            /// Determines if a PQL has items logged as packaged to it in the supplied pack log and returns those items.
            /// </summary>
            /// <param name="packlog">The pack log to search.</param>
            /// <param name="pql">The PQL to search for.</param>
            /// <param name="pql_items">Any found entries.</param>
            /// <returns>True if any entries were found.</returns>
            static public bool PQLPreviouslyPackaged(string packlog, string pql, out DataTable pql_items)
            {
                bool found = false;
                DataTable pql_items_found = new DataTable();
                pql_items_found.Columns.AddRange(new DataColumn[] 
                { 
                    new DataColumn("SerialNumber", Type.GetType("System.String")),
                    new DataColumn("PQL", Type.GetType("System.String"))
                });
                                
                if (File.Exists(packlog))
                {
                    ConcurrentBag<DataRow> pql_item_bag = new ConcurrentBag<DataRow>();
                    string[] all_lines = File.ReadAllLines(packlog);
                    Parallel.For(0, all_lines.Length, i => {
                        string[] line_items = all_lines[i].Split('\t');
                        if (line_items[1] == pql)
                        {
                            DataRow dr = pql_items_found.NewRow();
                            dr["SerialNumber"] = line_items[0];
                            dr["PQL"] = line_items[1];
                            pql_item_bag.Add(dr);
                            found = true;
                        }
                    });
                    if (found)
                        pql_items_found = pql_items_found.AsEnumerable().Union(pql_item_bag.AsEnumerable()).CopyToDataTable();
                }
                pql_items = pql_items_found;
                return found;
            }

            /// <summary>
            /// Checks if a serial number exists in the pack log.
            /// </summary>
            /// <param name="packlog">The pack log to look in.</param>
            /// <param name="serialnumber">The serial number to look for.</param>
            /// <returns>True if serial was found.</returns>
            static public bool SerialNumberExists(string packlog, string serialnumber)
            {
                if (File.Exists(packlog))
                {
                    serialnumber = serialnumber.Trim();
                    ConcurrentBag<string> all_lines = new ConcurrentBag<string>(File.ReadAllLines(packlog));
                    return all_lines.AsParallel().Any(line => line.Contains(serialnumber));                   
                }
                else
                    return false;
            }

            /// <summary>
            /// Logs a pass to the pack log.
            /// </summary>
            /// <param name="packlog">The pack log to log to.</param>
            /// <param name="serialnumber">The serial number to log.</param>
            /// <param name="pql">The PQL that the serial number was packaged in.</param>
            /// <returns>True if successful.</returns>
            static public bool LogPass(string packlog, string serialnumber, string pql)
            {
                bool timed_out;
                return LogPass(packlog, serialnumber, pql, out timed_out);
            }

            /// <summary>
            /// Logs a pass to the pack log.
            /// </summary>
            /// <param name="packlog">The pack log to log to.</param>
            /// <param name="serialnumber">The serial number to log.</param>
            /// <param name="pql">The PQL that the serial number was packaged in.</param>
            /// <param name="timed_out">True if the operation failed due to timeout.</param>
            /// <param name="timeout_ms">The timeout threshold in milliseconds.</param>
            /// <returns>True if successful.</returns>
            static public bool LogPass(string packlog, string serialnumber, string pql, out bool timed_out, int timeout_ms = TIMEOUT_MS)
            {
                timed_out = false;

                // If it's been more than a half hour, Archive if necessary                
                if (ArchivalIsNecessary(ref LAST_ARCHIVAL, packlog))
                    ArchiveLog(packlog);

                // Make sure the serial number wasn't passed with any whitespace or newlines.
                serialnumber = serialnumber.Trim();

                /* 
                // In the event of network or i/o issues hanging up the app: default 3 sec timeout implemented.
                ManualResetEvent wait = new ManualResetEvent(false);
                Thread work = new Thread(new ThreadStart(() =>
                {
                    // LOGGING
                    // (Use \r\n instead of Environment.Newline just to be super sure that we know what type of newline is being written.)
                    string line = string.Format("{0}\t{1}\t{2}\r\n", serialnumber, pql, DateTime.Now);
                    File.AppendAllText(packlog, line, Encoding.Default);
                    wait.Set();
                }));
                work.Start();
                if (!wait.WaitOne(timeout_ms))
                {
                    work.Abort();
                    timed_out = true;
                }
                */

                // LOGGING
                // (Use \r\n instead of Environment.Newline just to be super sure that we know what type of newline is being written.)
                string line = string.Format("{0}\t{1}\t{2}\r\n", serialnumber, pql, DateTime.Now);
                File.AppendAllText(packlog, line, Encoding.Default);
                // VERIFICATION
                return SerialNumberExists(packlog, serialnumber);
            }
        }

        /// <summary>
        /// Provides easy IO operations with Master Pass text logs.  NOT intended for use with any other logs.
        /// </summary>
        static public class PassLog
        {
            static DateTime LAST_ARCHIVAL = new DateTime();

            /// <summary>
            /// Determines if specified serial number exists in the pass log file after attempting to clean the file's formatting.
            /// </summary>
            /// <param name="serialnumber">The serial number to search for.</param>
            /// <param name="passlog">The pass log file to search in.  Assumes application directory if none specified.</param>
            /// <returns>True if the serial number was found.  False if the serial number was not found.  No exceptions handled.</returns>
            static public bool SerialNumberExists(string serialnumber, string passlog)
            {
                // Clean the file.
                ConcurrentBag<string> cleanfiletxt = new ConcurrentBag<string>(CleanedFileText(passlog));
                // Make sure the serial number wasn't passed with any whitespace or newlines.
                serialnumber = serialnumber.Trim();
                // Look for it...
                return cleanfiletxt.AsParallel().Any(line => line.Contains(serialnumber));
            }

            /// <summary>
            /// Attempts to append the specified text as a single line to the specified log file.  Attempts to create the file if it does not yet exist.
            /// </summary>
            /// <param name="serialnumber">The line of text to append.</param>
            /// <param name="passlog">The text log file.  Assumes application directory if none specified.</param>
            /// <returns>True if line already exists or was written successfully.  False if failed to verify write.  No exceptions handled.</returns>
            static public bool LogPass(string serialnumber, string passlog)
            {
                bool timed_out;
                return LogPass(serialnumber, passlog, out timed_out);
            }

            /// <summary>
            /// Attempts to append the specified text as a single line to the specified log file.  Attempts to create the file if it does not yet exist.
            /// </summary>
            /// <param name="serialnumber">The line of text to append.</param>
            /// <param name="passlog">The text log file.  Assumes application directory if none specified.</param>
            /// <param name="timed_out">Whether or not the write operation timed out.</param>
            /// <param name="timeout_ms">Timeout for write operation in ms.  Default 3000.</param>
            /// <returns>True if line already exists or was written successfully.  False if failed to verify write.  No exceptions handled.</returns>
            static public bool LogPass(string serialnumber, string passlog, out bool timed_out, int timeout_ms = TIMEOUT_MS)
            {
                timed_out = false;

                // If it's been more than a half hour, Archive if necessary                
                if (ArchivalIsNecessary(ref LAST_ARCHIVAL, passlog))
                    ArchiveLog(passlog);

                // Make sure the serial number wasn't passed with any whitespace or newlines.
                serialnumber = serialnumber.Trim();
                // If the serial number already exists, just return true.
                // This should never happen if we are remembering to remove the serial number from the log at the beginning of any test.
                if (SerialNumberExists(serialnumber, passlog))
                {
                    return true;
                }
                else
                {
                    /*
                    // In the event of network or i/o issues hanging up the app: default 3 sec timeout implemented.
                    ManualResetEvent wait = new ManualResetEvent(false);
                    Thread work = new Thread(new ThreadStart(() =>
                    {
                        // LOGGING
                        // (Use \r\n instead of Environment.Newline just to be super sure that we know what type of newline is being written.)
                        File.AppendAllText(passlog, serialnumber + "\r\n", Encoding.Default);
                        wait.Set();
                    }));
                    work.Start();
                    if (!wait.WaitOne(timeout_ms))
                    {
                        work.Abort();
                        timed_out = true;
                    }
                    */

                    // LOGGING
                    // (Use \r\n instead of Environment.Newline just to be super sure that we know what type of newline is being written.)
                    File.AppendAllText(passlog, serialnumber + "\r\n", Encoding.Default);
                    // VERIFICATION
                    return SerialNumberExists(serialnumber, passlog);
                }
            }

            /// <summary>
            /// Attempts to remove the specified serial number from the specified pass log.  (If the file does not exist, it will not be created and true is returned.)
            /// </summary>
            /// <param name="serialnumber">The serial number to remove.</param>
            /// <param name="passlog">The pass log file to remove the serial number from.  Assumes application directory if none specified.</param>
            /// <returns>True if the serial number no longer exists in the log.  False if removal verification failed.  No exceptions handled.</returns>
            static public bool RemoveFromLog(string serialnumber, string passlog)
            {
                bool timed_out;
                return RemoveFromLog(serialnumber, passlog, out timed_out);
            }

            /// <summary>
            /// Attempts to remove the specified serial number from the specified pass log.  (If the file does not exist, it will not be created and true is returned.)
            /// </summary>
            /// <param name="serialnumber">The serial number to remove.</param>
            /// <param name="passlog">The pass log file to remove the serial number from.  Assumes application directory if none specified.</param>
            /// <param name="timed_out">Whether or not the write operation timed out.</param>
            /// <param name="timeout_ms">Timeout for write operation in ms.  Default 3000.</param>
            /// <returns>True if the serial number no longer exists in the log.  False if removal verification failed.  No exceptions handled.</returns>
            static public bool RemoveFromLog(string serialnumber, string passlog, out bool timed_out, int timeout_ms = TIMEOUT_MS)
            {
                timed_out = false;

                if (File.Exists(passlog))
                {
                    // If it's been more than a half hour, Archive if necessary                
                    if (ArchivalIsNecessary(ref LAST_ARCHIVAL, passlog))
                        ArchiveLog(passlog);

                    // Make sure the serial number wasn't passed with any whitespace or newlines.
                    serialnumber = serialnumber.Trim();
                    // Get a cleaned list of lines from the file.                
                    List<string> cleanfiletxt = CleanedFileText(passlog);
                    // REMOVAL
                    if (cleanfiletxt.RemoveAll(line => line == serialnumber) > 0)
                    {
                        /*
                        // In the event of network or i/o issues hanging up the app: default 3 sec timeout implemented.
                        ManualResetEvent wait = new ManualResetEvent(false);
                        Thread work = new Thread(new ThreadStart(() =>
                        {
                            // Overwrite the file with the new list of lines.
                            File.WriteAllLines(passlog, cleanfiletxt.ToArray(), Encoding.Default);
                            wait.Set();
                        }));
                        work.Start();
                        if (!wait.WaitOne(timeout_ms))
                        {
                            work.Abort();
                            timed_out = true;
                        }
                        */

                        // Overwrite the file with the new list of lines.
                        File.WriteAllLines(passlog, cleanfiletxt.ToArray(), Encoding.Default);
                    }
                    // VERIFICATION
                    return !SerialNumberExists(serialnumber, passlog);
                }
                return true;
            }
        }

        /// <summary>
        /// Provides easy IO operations with Sync text logs.  NOT intended for use with any other logs.
        /// </summary>
        static public class SyncLog
        {
            static DateTime LAST_ARCHIVAL = new DateTime();

            /// <summary>
            /// Determines if specified subassembly serial number or final assembly serial number exists in the sync log file after attempting to clean the file's formatting.
            /// </summary>
            /// <param name="eitherserialnumber">The subassembly or final serial number to search for.</param>
            /// <param name="synclog">The sync log file to search in.  Assumes application directory if none specified.</param>
            /// <returns>True if the subassembly or final serial number was found.  False if the subassembly or final serial number was not found.  No exceptions handled.</returns>
            static public bool SerialNumberExists(string serialnumber, string synclog)
            {
                // Get a clean version of the sync log text.
                ConcurrentBag<string> cleanfiletxt = new ConcurrentBag<string>(CleanedFileText(synclog));
                // Make sure the serial number wasn't passed with any whitespace or newlines.
                serialnumber = serialnumber.Trim();
                // Search...
                return cleanfiletxt.AsParallel().Any(line => line.Contains(serialnumber));
            }

            /// <summary>
            /// Determines if specified subassembly serial number and final assembly serial number pair exists in the sync log file after attempting to clean the file's formatting.
            /// </summary>
            /// <param name="subserialnumber">The subassembly serial number member of the pair to search for.</param>
            /// <param name="finalserialnumber">The final assembly serial number member of the pair to search for.</param>
            /// <param name="synclog">The sync log file to search in.  Assumes application directory if none specified.</param>
            /// <returns>True if the pair was found.  False if the pair was not found.  No exceptions handled.</returns>
            static public bool PairExists(string subserialnumber, string finalserialnumber, string synclog)
            {
                // Clean the file.
                ConcurrentBag<string> cleanfiletxt = new ConcurrentBag<string>(CleanedFileText(synclog));
                // Make sure the subassembly serial number wasn't passed with any whitespace or newlines.
                subserialnumber = subserialnumber.Trim();
                // Make sure the final assembly serial number wasn't passed with any whitespace or newlines.
                finalserialnumber = finalserialnumber.Trim();
                // Form the entry to look for
                string pair = subserialnumber + "\t" + finalserialnumber;
                // Result
                return cleanfiletxt.AsParallel().Any(line => line.Contains(pair));
            }

            /// <summary>
            /// Determines if an exact tab-separated entry or 2 or more items exists in the sync log file after attempting to clean the file's formatting.
            /// </summary>
            /// <param name="entry">The exact entry of 2 or more tab-separated items to look for.</param>
            /// <param name="synclog">The sync log file to search in.</param>
            /// <returns>True if the entry was found.  No exceptions handled.</returns>
            static public bool EntryExists(string entry, string synclog)
            {
                // Clean the file.
                ConcurrentBag<string> cleanfiletxt = new ConcurrentBag<string>(CleanedFileText(synclog));
                // Make sure the entry passed has no leading or trailing whitespace or newlines.
                entry = entry.Trim();
                // Result
                return cleanfiletxt.AsParallel().Any(line => line == entry);
            }

            /// <summary>
            /// Retrieves an entry's synced value pairing.
            /// </summary>
            /// <param name="serialnumber">Serial number to look up.</param>
            /// <param name="synclog">Sync log to query.</param>
            /// <returns>Empty if no pair was found, otherwise, the serialnumber's paired entry.</returns>
            static public string GetPairedEntry(string serialnumber, string synclog)
            {
                // Prep the return value
                string pairedentry = "";
                // Clean the sync log
                ConcurrentBag<string> cleanfiletxt = new ConcurrentBag<string>(CleanedFileText(synclog));
                // Clean the lookup value
                serialnumber = serialnumber.Trim();
                // Query
                pairedentry = cleanfiletxt.AsParallel().FirstOrDefault(line => line.Contains(serialnumber));
                // If a pair was found, remove the lookup value and any whitespace or further tab-separated data to isolate the paired entry
                if (pairedentry != string.Empty)
                    pairedentry = pairedentry.Replace(serialnumber, string.Empty).Split('\t')[0].Trim();
                // Result
                return pairedentry;
            }

            /// <summary>
            /// Attempts to append the specified subassembly serial number and final assembly serial number pair (separated by a single tab) to the sync log.  Attempts to create the file if it does not yet exist.
            /// </summary>
            /// <param name="subserialnumber">The subassembly serial number member of the pair.</param>
            /// <param name="finalserialnumber">The final assembly serial number member of the pair.</param>
            /// <param name="synclog">The sync log file.  Assumes application directory if none specified.</param>
            /// <returns>True if pair already exists or was written successfully.  False if failed to verify write.  No exceptions handled.</returns>
            static public bool Sync(string subserialnumber, string finalserialnumber, string synclog)
            {
                bool timed_out;
                return Sync(subserialnumber, finalserialnumber, synclog, out timed_out);
            }

            /// <summary>
            /// Attempts to append the specified array of values as a single tab-separated entry to the sync log.  Attempts to create the file if it does not yet exist.
            /// </summary>
            /// <param name="entry_items">The ordered array of items to be entered as a single tab-separated entry.</param>
            /// <param name="synclog">The sync log file.  Assumes application directory if none specified.</param>
            /// <returns>True if entry already exists or was written successfully.  False if failed to verify write.  No exceptions handled.</returns>
            static public bool Sync(string[] entry_items, string synclog)
            {
                return Sync(entry_items.ToList(), synclog);
            }

            /// <summary>
            /// Attempts to append the specified list of values as a single tab-separated entry to the sync log.  Attempts to create the file if it does not yet exist.
            /// </summary>
            /// <param name="entry_items">The ordered list of items to be entered as a single tab-separated entry.</param>
            /// <param name="synclog">The sync log file.  Assumes application directory if none specified.</param>
            /// <returns>True if entry already exists or was written successfully.  False if failed to verify write.  No exceptions handled.</returns>
            static public bool Sync(List<string> entry_items, string synclog)
            {
                bool timed_out;
                return Sync(entry_items, synclog, out timed_out);
            }

            /// <summary>
            /// Attempts to append the specified subassembly serial number and final assembly serial number pair (separated by a single tab) to the sync log.  Attempts to create the file if it does not yet exist.
            /// </summary>
            /// <param name="entry_items">The subassembly serial number member of the pair.</param>
            /// <param name="finalserialnumber">The final assembly serial number member of the pair.</param>
            /// <param name="passlog">The sync log file.  Assumes application directory if none specified.</param>
            /// <param name="timed_out">Whether or not the write operation timed out.</param>
            /// <param name="timeout_ms">Timeout for write operation in ms.  Default 3000.</param>
            /// <returns>True if pair already exists or was written successfully.  False if failed to verify write.  No exceptions handled.</returns>
            static public bool Sync(string subserialnumber, string finalserialnumber, string synclog, out bool timed_out, int timeout_ms = TIMEOUT_MS)
            {
                timed_out = false;
                List<string> entry_items = new List<string>();

                // Make sure the subassembly serial number wasn't passed with any whitespace or newlines.
                subserialnumber = subserialnumber.Trim();
                // Make sure the final assembly serial number wasn't passed with any whitespace or newlines.
                finalserialnumber = finalserialnumber.Trim();

                entry_items.Add(subserialnumber);
                entry_items.Add(finalserialnumber);

                return Sync(entry_items, synclog, out timed_out, timeout_ms);
            }

            /// <summary>
            /// Attempts to append the specified subassembly serial number and final assembly serial number pair (separated by a single tab) to the sync log.  Attempts to create the file if it does not yet exist.
            /// </summary>
            /// <param name="entry_items">The ordered array of items to be entered as a single tab-separated entry.</param>
            /// <param name="synclog">The sync log file.  Assumes application directory if none specified.</param>
            /// <param name="timed_out">Whether or not the write operation timed out.</param>
            /// <param name="timeout_ms">Timeout for write operation in ms.  Default 3000.</param>
            /// <returns>True if entry already exists or was written successfully.  False if failed to verify write.  No exceptions handled.</returns>
            static public bool Sync(string[] entry_items, string synclog, out bool timed_out, int timeout_ms = TIMEOUT_MS)
            {
                timed_out = false;

                return Sync(entry_items.ToList(), synclog, out timed_out, timeout_ms);
            }

            /// <summary>
            /// Attempts to append the specified subassembly serial number and final assembly serial number pair (separated by a single tab) to the sync log.  Attempts to create the file if it does not yet exist.
            /// </summary>
            /// <param name="entry_items">The ordered list of items to be entered as a single tab-separated entry.</param>
            /// <param name="synclog">The sync log file.  Assumes application directory if none specified.</param>
            /// <param name="timed_out">Whether or not the write operation timed out.</param>
            /// <param name="timeout_ms">Timeout for write operation in ms.  Default 3000.</param>
            /// <returns>True if entry already exists or was written successfully.  False if failed to verify write.  No exceptions handled.</returns>
            static public bool Sync(List<string> entry_items, string synclog, out bool timed_out, int timeout_ms = TIMEOUT_MS)
            {
                timed_out = false;

                // If it's been more than a half hour, Archive if necessary                
                if (ArchivalIsNecessary(ref LAST_ARCHIVAL, synclog))
                    ArchiveLog(synclog);

                // Form a single tab-separated entry with no extra whitespace anywhere
                foreach (string item in entry_items)
                {
                    item.Trim();
                    DateTime date = new DateTime();
                    if (!DateTime.TryParse(item, out date))
                        item.Replace(" ", "");
                }
                string new_entry = string.Join("\t", entry_items).Trim() + "\r\n";

                // If the entry already exists, just return true
                if (EntryExists(new_entry, synclog))
                {
                    return true;
                }
                else
                {
                    /*
                    // In the event of network or i/o issues hanging up the app: default 3 sec timeout implemented.
                    ManualResetEvent wait = new ManualResetEvent(false);
                    Thread work = new Thread(new ThreadStart(() =>
                    {
                        // LOGGING
                        // (Use \r\n instead of Environment.Newline just to be super sure that we know what type of newline is being written.)
                        File.AppendAllText(synclog, new_entry, Encoding.Default);
                        wait.Set();
                    }));
                    work.Start();
                    if (!wait.WaitOne(timeout_ms))
                    {
                        work.Abort();
                        timed_out = true;
                    }
                    */

                    // LOGGING
                    // (Use \r\n instead of Environment.Newline just to be super sure that we know what type of newline is being written.)
                    File.AppendAllText(synclog, new_entry, Encoding.Default);
                    // VERIFICATION
                    return EntryExists(new_entry, synclog);
                }
            }

            /// <summary>
            /// Attempts to remove any pairs where the specified subassembly or final serial number is a member from the specified pass log.  (If the file does not exist, it will not be created and true is returned.)
            /// </summary>
            /// <param name="entry_item">Any log entry item.</param>
            /// <param name="synclog">The sync log file to remove the serial number from.  Assumes application directory if none specified.</param>
            /// <param name="timed_out">Whether or not the write operation timed out.</param>
            /// <param name="timeout_ms">Timeout for write operation in ms.  Default 3000.</param>
            /// <returns>True if the serial number no longer exists in the log.  False if removal verification failed.  No exceptions handled.</returns>
            static public bool RemoveFromLog(string entry_item, string synclog)
            {
                bool timed_out;
                return RemoveFromLog(entry_item, synclog, out timed_out);
            }

            /// <summary>
            /// Attempts to remove any pairs where the specified subassembly or final serial number is a member from the specified pass log.  (If the file does not exist, it will not be created and true is returned.)
            /// </summary>
            /// <param name="entry_item">Any log entry item.</param>
            /// <param name="synclog">The sync log file to remove the serial number from.  Assumes application directory if none specified.</param>
            /// <returns>True if the serial number no longer exists in the log.  False if removal verification failed.  No exceptions handled.</returns>
            static public bool RemoveFromLog(string entry_item, string synclog, out bool timed_out, int timeout_ms = TIMEOUT_MS)
            {
                timed_out = false;

                // If it's been more than a half hour, Archive if necessary                
                if (ArchivalIsNecessary(ref LAST_ARCHIVAL, synclog))
                    ArchiveLog(synclog);

                if (File.Exists(synclog))
                {
                    // Make sure the serial number wasn't passed with any whitespace or newlines.
                    entry_item = entry_item.Trim();
                    // Get a cleaned list of lines from the log.
                    List<string> cleanfiletxt = CleanedFileText(synclog);
                    // REMOVAL
                    if (cleanfiletxt.RemoveAll(line => line.Contains(entry_item)) > 0)
                    {
                        /*
                        // In the event of network or i/o issues hanging up the app: default 3 sec timeout implemented.
                        ManualResetEvent wait = new ManualResetEvent(false);
                        Thread work = new Thread(new ThreadStart(() =>
                        {
                            // Overwrite the file with the new list of lines.
                            File.WriteAllLines(synclog, cleanfiletxt.ToArray(), Encoding.Default);
                            wait.Set();
                        }));
                        work.Start();
                        if (!wait.WaitOne(timeout_ms))
                        {
                            work.Abort();
                            timed_out = true;
                        }
                        */

                        // Overwrite the file with the new list of lines.
                        File.WriteAllLines(synclog, cleanfiletxt.ToArray(), Encoding.Default);
                    }
                    // VERIFICATION
                    return !SerialNumberExists(entry_item, synclog);
                }
                return true;
            }
        }

        static public bool TimeToCheckForArchival(DateTime last_archival)
        {
            double min_elapsed = ((TimeSpan)(DateTime.Now - last_archival)).TotalMinutes;
            if (min_elapsed >= 30)
                return true;
            else
                return false;
        }

        static public bool ArchivalIsNecessary(ref DateTime last_archival, string log)
        {
            if (File.Exists(log))
            {
                if (TimeToCheckForArchival(last_archival))
                {
                    if (File.ReadAllLines(log).ToList().Count >= ARCHIVE_LINE_COUNT_THRESHOLD)
                    {
                        last_archival = DateTime.Now;
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Attempts to archive entries in a log file based on IDS.TextLogIO.ARCHIVE_LINE_COUNT_THRESHOLD (when file is big enough to require an archival)
        /// and IDS.TextLogIO.ARCHIVE_LINE_COUNT_CROP (how many lines to leave in the master log).
        /// </summary>
        /// <param name="log">The log file.</param>
        /// <param name="timeout_ms">The timeout in milliseconds.</param>
        static public void ArchiveLog(string log, int timeout_ms = TIMEOUT_MS)
        {
            try
            {
                string archive = "Archive";
                if (!log.ToUpper().Contains(archive.ToUpper()))
                {
                    List<string> cleaned_lines = CleanedFileText(log);
                    string root_path = new FileInfo(log).DirectoryName;
                    if (root_path.Substring(root_path.Length) != "\\")
                        root_path += "\\";
                    string archive_path = root_path + archive + "\\";
                    if (!Directory.Exists(archive_path))
                        Directory.CreateDirectory(archive_path);                    
                    int archive_range = cleaned_lines.Count - ARCHIVE_LINE_COUNT_CROP;

                    /*
                    ManualResetEvent wait = new ManualResetEvent(false);
                    Thread work = new Thread(new ThreadStart(() =>
                    {
                        File.AppendAllLines(archive_path + log, cleaned_lines.Take(archive_range).ToArray());
                        File.WriteAllLines(log, cleaned_lines.Skip(archive_range).ToArray());
                    }));
                    work.Start();
                    if (!wait.WaitOne(timeout_ms))
                    {
                        work.Abort();
                    }
                    */

                    File.AppendAllLines(archive_path + log, cleaned_lines.Take(archive_range).ToArray());
                    File.WriteAllLines(log, cleaned_lines.Skip(archive_range).ToArray());
                }
            }
            catch
            { 
                // Nothing for now
            }
        }

        /// <summary>
        /// Attempts to clean the formatting of the text file so that there are no blank lines other than the last one, 
        /// that the last line is in fact blank, and that no lines have leading or trailing whitespace.
        /// </summary>
        /// <param name="txtfile">The file to clean.</param>
        /// <returns>List of cleanly formatted lines as strings.</returns>
        static public List<string> CleanedFileText(string txtfile, bool ispqllog = false)
        {
            // Initialize list for lines of text we will want to keep.
            List<string> cleanedlines = new List<string>();
            // Only proceed if the file even exists...
            if (File.Exists(txtfile))
            {
                /*--CLEANING--*/
                // Trim the whole file first (this will remove leading and trailing empty lines).
                string filetxt = File.ReadAllText(txtfile, Encoding.Default).Trim();
                // Read the entire contents of the file as an array of lines.
                var lines = filetxt.Split('\n');
                if (lines != null && lines.Length > 0)
                {
                    // Initialize regex search pattern to look for any whitespace.
                    // This will be used to account for the case of a sync log file which should have entries separated by single tabs with no more than 1 consecutive occurance of whitespace in a line.
                    Regex rgx = new Regex(@"\s{2,}");
                    // Keep track of the length of the first clean line for verification of every subsequent line thereafter.
                    int first_line_len = 0;
                    // For every line in the file...
                    for (int i = 0; i < lines.Length; i++)
                    {
                        // Trim the line to remove any leading or trailing whitespace (this includes CRLF).
                        lines[i] = lines[i].Trim();
                        // If the result isn't blank...
                        if (lines[i] != "")
                        {
                            // Make sure that any interior whitespace is reduced to a single tab (sync log entry).
                            lines[i] = rgx.Replace(lines[i], "\t");

                            /*--VERIFICATION--*/
                            // If the line is blank, then cleaning failed.
                            if (String.IsNullOrEmpty(lines[i].Trim()))
                                throw new BadLogFileFormattingException(string.Format("Line {0} is blank. (File: {1}).", (i + 1), txtfile));
                            // If the line has leading or trailing whitespace, then cleaning failed.
                            if (lines[i] != lines[i].Trim())
                                throw new BadLogFileFormattingException(string.Format("Line {0} has leading or trailing white space. (File: {1}).", (i + 1), txtfile));
                            // If this is not a PQL log, then we care about line length being relatively uniform
                            if (!ispqllog)
                            {
                                // If this is the first line and we've gotten this far which means it is cleaned (and therefor not zero length), then record its length for subsequent line comparisons.
                                if (i == 0)
                                    first_line_len = lines[i].Length;
                                // If this is not the first line, then compare its length to the first line for final verification.
                                // 5 character leeway is allowed.
                                else if (lines[i].Length != first_line_len & (lines[i].Length > first_line_len + 5 | lines[i].Length < first_line_len - 5))
                                    throw new BadLogFileFormattingException(string.Format("Line {0} is not the proper length. (File: {1}).", (i + 1), txtfile));
                            }
                            // If we get this far without an exception, it is fully cleaned and verified so add it to the list of keepers.
                            cleanedlines.Add(lines[i]);
                        }
                    }
                }
            }
            // Returns either an empty list if the file was not found or empty, or a list of cleaned lines from the file.
            return cleanedlines;
        }

        /// <summary>
        /// Attempts to clean the formatting of the text file so that there are no blank lines other than the last one, 
        /// that the last line is in fact blank, and that no lines have leading or trailing whitespace.
        /// </summary>
        /// <param name="txtfile">The file to clean.</param>
        /// <returns>List of cleanly formatted lines as strings.</returns>
        static public List<string> CleanedSyncBOM(string txtfile)
        {
            // Initialize list for lines of text we will want to keep.
            List<string> cleanedlines = new List<string>();
            // Only proceed if the file even exists...
            if (File.Exists(txtfile))
            {
                /*--CLEANING--*/
                // Trim the whole file first (this will remove leading and trailing empty lines).
                string filetxt = File.ReadAllText(txtfile, Encoding.Default).Trim();
                // Read the entire contents of the file as an array of lines.
                var lines = filetxt.Split('\n');
                if (lines != null && lines.Length > 0)
                {
                    // Initialize regex search pattern to look for any whitespace.
                    Regex rgx = new Regex(@"\s+");
                    // For the first two lines in the file (all we would ever care about)
                    for (int i = 0; i < 2; i++)
                    {
                        // Trim the line to remove any leading or trailing whitespace (this includes CRLF).
                        lines[i] = lines[i].Trim();
                        // If the result isn't blank...
                        if (lines[i] != "")
                        {
                            // Make sure that any interior whitespace is reduced to a single space.
                            lines[i] = rgx.Replace(lines[i], " ");

                            /*--VERIFICATION--*/
                            // If the line is blank, then cleaning failed.
                            if (String.IsNullOrEmpty(lines[i].Trim()))
                                throw new BadLogFileFormattingException(string.Format("Line {0} is blank. (File: {1}).", (i + 1), txtfile));
                            // If the line has leading or trailing whitespace, then cleaning failed.
                            if (lines[i] != lines[i].Trim())
                                throw new BadLogFileFormattingException(string.Format("Line {0} has leading or trailing white space. (File: {1}).", (i + 1), txtfile));
                            // Get each item in the line individually
                            var line_items = new List<string>(lines[i].Split(' '));
                            // reconstruct the line with a single tab after the first item (part number)
                            string part = line_items[0];
                            line_items.RemoveAt(0);
                            // if this is the second line, and the part number is not the same as the text file name, fail cleaning.
                            if (i == 1 && !txtfile.Split('.')[0].EndsWith(part))
                                throw new BadLogFileFormattingException(string.Format("Line {0} is not the assembly part number. (File: {1}).", (i + 1), txtfile));
                            // add back all the other items (description) as a sentance                            
                            string reconstructed = part + "\t" + string.Join(" ", line_items);
                            // If we get this far without an exception, it is fully cleaned and verified so add it to the list of keepers.
                            cleanedlines.Add(reconstructed);
                        }
                    }
                }
            }
            // Returns either an empty list if the file was not found or empty, or a list of cleaned lines from the file.
            return cleanedlines;
        }

        /// <summary>
        /// Attempts to clean the formatting of the text file so that there are no blank lines other than the last one, 
        /// that the last line is in fact blank, and that no lines have leading or trailing whitespace.
        /// </summary>
        /// <param name="txtfile">The file to clean.</param>
        /// <returns>List of cleanly formatted lines as strings.</returns>
        static public List<string> CleanedPackBOM(string txtfile)
        {
            // Initialize list for lines of text we will want to keep.
            List<string> cleanedlines = new List<string>();
            // Only proceed if the file even exists...
            if (File.Exists(txtfile))
            {
                /*--CLEANING--*/
                // Trim the whole file first (this will remove leading and trailing empty lines).
                string filetxt = File.ReadAllText(txtfile, Encoding.Default).Trim();
                // Read the entire contents of the file as an array of lines.
                var lines = filetxt.Split('\n');
                if (lines != null && lines.Length > 0)
                {
                    // Initialize regex search pattern to look for any whitespace.
                    // This will be used to account for the case of a sync log file which should have a single tab surrounded by text on either side.
                    Regex rgx = new Regex(@"\s+");
                    // For every line in the file...
                    for (int i = 0; i < lines.Length; i++)
                    {
                        // Trim the line to remove any leading or trailing whitespace (this includes CRLF).
                        lines[i] = lines[i].Trim();
                        // If the result isn't blank...
                        if (lines[i] != "")
                        {
                            // Make sure that any interior whitespace is reduced to a single space.
                            lines[i] = rgx.Replace(lines[i], " ");

                            /*--VERIFICATION--*/
                            // If the line is blank, then cleaning failed.
                            if (String.IsNullOrEmpty(lines[i].Trim()))
                                throw new BadLogFileFormattingException(string.Format("Line {0} is blank. (File: {1}).", (i + 1), txtfile));
                            // If the line has leading or trailing whitespace, then cleaning failed.
                            if (lines[i] != lines[i].Trim())
                                throw new BadLogFileFormattingException(string.Format("Line {0} has leading or trailing white space. (File: {1}).", (i + 1), txtfile));
                            // Get each item in the line individually
                            var line_items = new List<string>(lines[i].Split(' '));
                            // reconstruct the line with a single tab after the first item (part number)
                            string part = line_items[0];
                            line_items.RemoveAt(0);
                            // if this is the first line, and the part number is not the same as the text file name, fail cleaning.
                            if (i == 0 && !txtfile.Split('.')[0].EndsWith(part))
                                throw new BadLogFileFormattingException(string.Format("Line {0} is not the assembly part number. (File: {1}).", (i + 1), txtfile));
                            // add back all the other items (description) as a sentance                            
                            string reconstructed = part + "\t" + string.Join(" ", line_items);
                            // If we get this far without an exception, it is fully cleaned and verified so add it to the list of keepers.
                            cleanedlines.Add(reconstructed);
                        }
                    }
                }
            }
            // Returns either an empty list if the file was not found or empty, or a list of cleaned lines from the file.
            return cleanedlines;
        }

        /// <summary>
        /// Used to convey custom messages when formatting violations are found during the format cleaning of a log file.
        /// </summary>
        public class BadLogFileFormattingException : ApplicationException
        {
            public BadLogFileFormattingException() { }
            public BadLogFileFormattingException(string message) : base(message) { }
            public BadLogFileFormattingException(string message, Exception inner) : base(message, inner) { }
        }
    }
}