using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}