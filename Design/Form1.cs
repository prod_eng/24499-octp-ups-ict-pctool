// if defined, code for function tester is built, otherwise code for final tester is built
// function tester - scan PCBA to start test
// final tester - scan barcode on outside label to start test
#define FunctionTest

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool
{
    public partial class Form1 : IDS.CommShareForm
    {
        // test parameters
        const String LabelNumber = "22438A"; // label bar code
        const double DefaultTimeout = 7.0; // seconds
        
        // IDS.Timer IDSStateTime = new IDS.Timer();

        DateTime TestTime; // time that test was started

        string BarCodeScanned = "";

        UInt16[] gu16AnalogReadings;
        public StateMachineClass gSM  = new StateMachineClass();


        public Form1()
        {
            InitializeComponent();

            Text = IDS.AppInfo.Title;
            IDS.Form gIdsForm = new IDS.Form();
            gu16AnalogReadings = new UInt16[ 40 ];
            gSM  = new StateMachineClass();


        // add about box to system menu
        IDS.Form.SysMenu menu = gIdsForm.SystemMenu;

            menu.AppendSeparator();
            menu.AppendMenu("About...", new IDS.Form.SysMenu.Handler(delegate { new IDS.AboutBox().ShowDialog(this); }));

            // initialize status bar
            PortStatusLabel.Text = "";

            // initialize bar-code reader to scan for assembly barcode label
            IDS.BarCodeReader.Add(LabelNumber + "dddddddddd", this.OnBarCodeScanned);
            FaceLabel.Text = "";

        }

        public String HistoryText = "";
        public String History
        {
            set
            {
                HistoryText = value;
                StatusLabel.Text = value;
                HistoryBox.Items.Add(value);

                // force item to become visible
                int itemsPerPage = (int)(HistoryBox.Height / HistoryBox.ItemHeight);
                HistoryBox.TopIndex = HistoryBox.Items.Count - itemsPerPage;
            }
        }

        public void SendMessage( byte[] message )
        {
            RS232_Send( message );
        }

        void OnBarCodeScanned(String match, String text)
        {
            //switch (State)
            //{
            //    case STATE.Ready:
            //    case STATE.ScanAssemblyNumber:
            //        BarCodeScanned = text;
            //        State = STATE.ScanAssemblyNumber;
            //        break;
            //}

            gSM.gTester.vInformTesterOfDUTConfig();
            gSM.vTransToReady();
        }

        
        private void timer1_Tick(object sender, EventArgs e)
        {
            // pump the state machine
            gSM.gTester.vSendAskSensor( 0 );
            gSM.vRunCurrentState();
            label1.Text = gSM.eCurrentState.ToString();
        }

                  
        private void Form1_RS232_Closed()
        {
            gSM.vTransToNoComm();
            // MJS PortStatusLabel.Text = "";
        }

        private void Form1_RS232_Opened( int portnumber, int baudrate )
        {
            gSM.vTransToReady();
            // MJS PortStatusLabel.Text = "COM" + portnumber.ToString() + ", " + baudrate.ToString() + " baud";
        }

        private void Form1_RS232_Rx( IDS.CommShareForm.RxMessage message )
        {
            vProcessTesterReply( message.Data );
        }


#region Tester Reply processing
        public void vProcessTesterReply( byte[] u8Packet )
        {
            switch( u8Packet[ 0 ] )
            {
                case 0x00: // Tester heartbeat
                    break;

                case 0x04: // Read output millivolts
                    gu16AnalogReadings[ u8Packet[ 1 ] ] = (UInt16)( ( 256 * ( u8Packet[ 2 ] ) ) + u8Packet[ 3 ] );

                    switch( u8Packet[ 1 ] )
                    {

                        case 0: // If this is our cap voltage reading
                            gSM.vProcessNewReading( gu16AnalogReadings[ 0 ] );
                            break;

                        case 1: // If this is our VBatt voltage reading
                            break;
                    }
                    break;
            }
        }

        private void button1_Click( object sender, EventArgs e )
        {
            gSM.vTransToStart();
        }


        #endregion Tester Reply processing



        //private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    StringBuilder s = new StringBuilder();
        //    foreach (String t in HistoryBox.Items)
        //        s.AppendLine(t);
        //    if (s.Length > 0)
        //        Clipboard.SetText(s.ToString());
        //}

        //void UpdateLogFile(bool passed)
        //{
        //    String filename = IDS.AppInfo.ProductName + " " + TestTime.Year.ToString("0000") + TestTime.Month.ToString("00") + TestTime.Day.ToString("00") + ".txt";

        //    // see if the file exists
        //    bool write_header = !File.Exists(filename);

        //    // create/append to the file
        //    FileStream stream = new FileStream(filename, FileMode.Append, FileAccess.Write);
        //    StreamWriter writer = new StreamWriter(stream);

        //    string tab = "\t";

        //    // write header
        //    if (write_header)
        //    {
        //        string h = "Failed";
        //        h += tab + "Time";
        //        h += tab + "Assembly number";
        //        h += tab + "Test result";
        //        writer.WriteLine(h);
        //    }

        //    string log = passed ? " " : "*";
        //    log += tab + TestTime.Hour.ToString("00") + ":" + TestTime.Minute.ToString("00") + ":" + TestTime.Second.ToString("00");
        //    log += tab + BarCodeScanned;
        //    log += tab + StatusLabel.Text;
        //    if (!passed)
        //        log += " (Tester = $" + FailState.ToString("X").Substring(2) + ")";
        //    writer.WriteLine(log);

        //    writer.Close();
        //    stream.Close();
        //}

        ///// <summary>
        ///// Add/Remove text in the master log file
        ///// </summary>
        ///// <param name="text">text to add</param>
        ///// <param name="addtofile">true if text should be added, false if text should be removed</param>
        //void UpdateMasterLogFile(String text, bool add)
        //{            
        //    try
        //    {
        //        String filename = IDS.AppInfo.ProductName + ".txt";
        //        if (add)
        //            IDS.TextLogIO.PassLog.LogPass(text, filename);
        //        else
        //            IDS.TextLogIO.PassLog.RemoveFromLog(text, filename);
        //    }
        //    catch
        //    {
        //    }
        //}
    }
}
