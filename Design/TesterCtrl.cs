﻿using System;

namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool
{

    public partial class TesterControlClass : Form1
    {

        public void vDelayTicks( UInt32 delayTicks )
        {
            long u32DelayUntil = DateTime.Now.Ticks + ( 17 * delayTicks ); // ported from system where tick rate was 60Hz. ( so 16.6667 mS / tick) 17 is close enough for this 
            while( DateTime.Now.Ticks < u32DelayUntil )
            {
                // Spin
            }
            
            return;
        }


        public void vSendAskSensor( byte u8WhichTesterSensor )
        {
            byte[] u8MessageToSend = new byte[ 2 ]
            {
                0x04,                   // 0 Read Tester Sensor Command 
                u8WhichTesterSensor     // 1 Which sensor
            };

            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            return;
        }


        public void vChargeCap()
        {
            vStopDischarging();

            byte[] u8MessageToSend = new byte[ 3 ]
            {
                0x05,   // 0 Set Stimulus Command 
                0x04,   // 1 Which stim to charge cap
                0x01    // Turn it ON.
            };
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            return;
        }


        public void vDischargeCap()
        {
            vStopCharging();

            byte[] u8MessageToSend = new byte[ 3 ]
            {
                0x05,   // 0 Set Stimulus Command 
                0x17,   // 1 Which stim to slow-discharge cap
                0x01    // Turn it ON.
            };
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            return;
        }


        public void vDrainCap()
        {
            byte[] u8MessageToSend = new byte[ 3 ]
            {
                0x05,   // 0 Set Stimulus Command 
                0x18,   // 1 Which stim to fast-discharge the cap
                0x01    // Turn it ON.
            };
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            return;
        }


        public void vInformTesterOfDUTConfig()
        {
            byte[] u8MessageToSend = new byte[ 3 ]
            {
                0x06,   // 0 Inform Product to Test
                0x01,   // 1 Variant
                0x00    // 2 Rev 0
            };
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            return;
        }


        public void vMaxCharge()
        {
            byte[] u8MessageToSend = new byte[ 3 ]
            {
                0x05,   // 0 Set Stim
                0x1e,   // 1 Which stim to Power the DUT
                0x01    // Turn it ON.
            };
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            return;
        }


        public void vStopCharging()
        {
            byte[] u8MessageToSend = new byte[ 3 ]
            {
                0x05,   // 0 Set Stimulus Command 
                0x1e,   // 1 Which stim is fast-charging the cap (DUT Power)
                0x00    // 2 Turn it OFF.
            };
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            u8MessageToSend[ 1 ] = 0x04; // Which stim is slow-charging the Cap
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            return;
        }


        public void vStopDischarging()
        {
            byte[] u8MessageToSend = new byte[ 3 ]
            {
                0x05,   // 0 Set Stimulus Command 
                0x18,   // 1 The fast-discharge Stim
                0x00    // 2 Turn it OFF.
            };
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            u8MessageToSend[ 1 ] = 0x17;            // The slow-discharge Stim
            SendMessage( u8MessageToSend ); // Send to tester board

            vDelayTicks( 10 );

            return;
        }


        
    } // end of TesterControlClass

} // end of namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool


