﻿using System;

namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool
{
    public class DUTClass : IDisposable
    {
        public String gstrDutSerial = "";
        public double dblCapVoltage = 0.0;
        public double dblElapsed = 0.0;
        public double[] gTransitionTimes = new double[(int)StateMachineClass.eTESTING_STATE_TYPE.eTST_STATE_COUNT];
        public DateTime dateTenVoltTime;

        public DUTClass( string DUTSerial ) // Constructor
        {
            gstrDutSerial = DUTSerial;

            return;
        }

        ~DUTClass() // Destructor
        {

        }

        public void Dispose()
        {
            // MJS release resources and clean up
        }

    } // end of "class DUTClass"

} // end of namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool
