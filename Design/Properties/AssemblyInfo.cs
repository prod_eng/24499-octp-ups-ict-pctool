﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "24499-A OCTP Uninterruptable Power Supply ICT PC Tool" )]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("IDS Electronics a division of Lippert Components, Inc.\r\n6801 15 Mile Rd\r\nSterling Heights, MI 48312\r\nwww.lci1.com")]
[assembly: AssemblyProduct( "24499 OCTP Uninterruptable Power Supply ICT PC Tool" )]
[assembly: AssemblyCopyright("Copyright © 2018 Lippert Components, Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid( "36dbf56f-2eeb-49ef-a40c-3469b289ba4d" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
