﻿using System;
using System.Collections.Generic;

namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool
{
    public delegate void pvFnSTATE_RUNNER_TYPE();

    public partial class StateMachineClass : Form1
    {
        private const double cCHARGE_BEGIN_TOPOFF_VOLTAGE = 11.0;
        private const double cDISCHARGE_DONE_VOLTAGE = 4.0;
        private const double cDISCHARGE_START_VOLTAGE = 10.0;
        private const double cMAX_DISCHARGE_WAIT = 14.0;
        private const double cMAX_PASSING_TIME = 5.5; // Seconds
        private const double cMAX_VBATT = 14.25; // Volts
        private const double cMIN_PASSING_TIME = 5.3; // Seconds
        private const double cMIN_VBATT = 13.25; // Volts
        private const double cTIMEOUT_TIME = 15.0;

        public enum eTESTING_STATE_TYPE
        {
            eTST_STATE_NO_COMM = 0,
            eTST_STATE_READY,
            eTST_STATE_VALIDATE_SCAN,
            eTST_STATE_CHARGE,
            eTST_STATE_DISCHARGING,
            eTST_STATE_EVALUATE,
            eTST_STATE_POST_RESULTS,
            eTST_STATE_FAILED,
            // New States above this line
            eTST_STATE_COUNT
        }
        public enum eFAIL_TYPE
        {
            eFAIL_TIMEOUT_CHARGING = 0,
            eFAIL_TIMEOUT_DISCHARGING,
            eFAIL_DISCHARGE_TOO_FAST,
            eFAIL_DISCHARGE_TOO_SLOW,
            eFAIL_COULD_NOT_LOG,
            // New Failurs above this line
            eFAIL_COUNT
        }
        public enum eTIMING_STATE_TYPE
        {
            eSTATE_READY = 0,
            eSTATE_COUNTING,
            eSTATE_STOPPED,
            eSTATE_LOCKED,
            // New states go above this line
            eSTATE_COUNT
        };
        public String gstrStateName = "";
        public String gstrFailText = "";



        public eTESTING_STATE_TYPE eCurrentState
        {
            get
            {
                return meCurrentState;
            }
            set
            {
                mStateTime = 0;
                dtLastReadTime = DateTime.Now;
                meCurrentState = value;

                if( eTESTING_STATE_TYPE.eTST_STATE_NO_COMM != meCurrentState )
                {
                    TimeSpan delta = ( DateTime.Now - dtLastReadTime );
                    // This array is only created after a new serial port is selected
                    gDUT.gTransitionTimes[ (int)value ] = ( delta.Milliseconds / 1000 );
                }
            }
        }

        public Double StateTime
        {
            get
            {
                TimeSpan delta = ( DateTime.Now - dtLastReadTime );
                mStateTime = ( ( delta.Milliseconds ) / 1000 );
                return mStateTime;
            }
        }

        private eTESTING_STATE_TYPE meCurrentState = eTESTING_STATE_TYPE.eTST_STATE_NO_COMM;
        private Double mStateTime = 0.0;
        private static DateTime dtLastReadTime = DateTime.Now;
        private eTIMING_STATE_TYPE eCounter = eTIMING_STATE_TYPE.eSTATE_STOPPED;
        public DUTClass gDUT = null;
        public TesterControlClass gTester = new TesterControlClass();
        
        private readonly String[] strFailNames = new String[(int)eFAIL_TYPE.eFAIL_COUNT]
        {
            "Timed out attempting to charge caps.",
            "Timed out attempting to discharge caps.",
            "Capacitor discharge was too fast.  Perhaps a cap missing or not soldered.",
            "Capacitor discharge was too slow.  Perhaps wrong cap values.",
            "Could not log results to database."
        };

        private readonly String[] strTableStates = new String[(int)eTESTING_STATE_TYPE.eTST_STATE_COUNT]
        {
            "No Serial Port Connection",
            "Ready",
            "Validate Barcode",
            "Charging",
            "Discharging",
            "Evaluating Results",
            "Posting results to database",
            "Failed"
        };

        void vFreeDUTObject()
        {
            if( null != gDUT )
            {
                gDUT.Dispose();
            }

            return;
        }


        public void vProcessNewReading( UInt16 u16Millivolts )
        {
            Double dblReading = ( u16Millivolts / 1000 );
            TimeSpan delta = ( DateTime.Now - gDUT.dateTenVoltTime );
            Double dblTime = ( delta.Milliseconds / 1000 );
            gDUT.dblCapVoltage = dblReading; // Update the local property 

            if( eTIMING_STATE_TYPE.eSTATE_COUNTING == eCounter )
            {
                // MJS MainWindow.vLogReading( dblReading, dblTime );
            }

            // MJS MainWindow.vDrawPoint( MainWindow.ScopeTrace.graphics, dblTime, dblReading );
            // MJS MainWindow.vDisplayCapVoltage( dblReading ); // Display it in the UI 

            return;
        }


        public void vRunCurrentState()
        {
            if( eCurrentState < eTESTING_STATE_TYPE.eTST_STATE_COUNT )
            {
                // Display the current state machine state and its description
                gstrStateName = " test"; // MJS
            }

            List<pvFnSTATE_RUNNER_TYPE> pvFnTableState = new List<pvFnSTATE_RUNNER_TYPE>
            {
                delegate { vStFn_NoComm();          },
                delegate { vStFn_Ready();           },
                delegate { vStFn_ValidateBarcode(); },
                delegate { vStFn_Charge();          },
                delegate { vStFn_Discharge();       },
                delegate { vStFn_EvaluateResult();  },
                delegate { vStFn_PostResults();     },
                delegate { vStFn_Failed();          }
            };

            // Execute the state function for the current state
            pvFnTableState[ (int)eCurrentState ]();


            return;
        }


        #region state functions

        public static void vStFn_NoComm()
        {
            return;
        }


        private void vStFn_Ready()
        {
            return;
        }


        private void vStFn_ValidateBarcode()
        {
            return;
        }


        private void vStFn_Charge()
        {
            if( StateTime > cTIMEOUT_TIME )
            {
                vTransToFail( eFAIL_TYPE.eFAIL_TIMEOUT_CHARGING );
            }

            if( gDUT.dblCapVoltage > cCHARGE_BEGIN_TOPOFF_VOLTAGE )
            {
                gTester.vMaxCharge();
            }

            if( gDUT.dblCapVoltage > (cMIN_VBATT - 0.2) )
            {
                vTransStartDischarge();
            }

            //  From Xojo Implementation:
            // 
            //  // Check for timeout charging
            //  If( StateTime > App.cTIMEOUT_TIME ) Then
            //      vTransToFail( eFAIL_TYPE.eFAIL_TIMEOUT_CHARGING )
            //  End If
            //
            //  // Look for mostly charged caps
            //  If( gDUT.dblCapVoltage > App.cCHARGE_BEGIN_TOPOFF_VOLTAGE ) Then
            //      TesterCtrl.vMaxCharge
            //  End If
            //
            //  // Look for fully charged
            //  If( gDUT.dblCapVoltage > ( App.cMIN_VBATT - 0.2 ) ) Then
            //      vTransStartDischarge
            //  End If

            return;
        }


        private void vStFn_Discharge()
        {
            if( gDUT.dblCapVoltage >= cDISCHARGE_START_VOLTAGE )
            {
                eCounter = eTIMING_STATE_TYPE.eSTATE_READY;
                gDUT.dblElapsed = 0.0;
            }
            else if( ( gDUT.dblCapVoltage < cDISCHARGE_START_VOLTAGE ) && ( gDUT.dblCapVoltage > ( cDISCHARGE_START_VOLTAGE - 0.2 ) ) ) // Give it a little range in case we didn't get a good reading
            {
                gDUT.dateTenVoltTime = DateTime.Now;
                eCounter = eTIMING_STATE_TYPE.eSTATE_COUNTING;
                // MainWindow.vDrawVertTimeMarker( ) // Update GUI with starting marker line
            }
            else if( eTIMING_STATE_TYPE.eSTATE_COUNTING == eCounter )
            {
                // We are in the critical measurement band
                if( gDUT.dblCapVoltage < cDISCHARGE_DONE_VOLTAGE )
                {
                    TimeSpan delta = ( DateTime.Now - gDUT.dateTenVoltTime );
                    gDUT.dblElapsed = ( delta.Milliseconds / 1000 );
                    eCounter = eTIMING_STATE_TYPE.eSTATE_STOPPED; // Stop measuring
                    // update GUI scope display
                    // MainWindow.vDrawVertTimeMarker( MainWindow.ScopeTrace.graphics, gDUT.dblElapsed )
                    // MainWindow.txtMeasuredTime.Text = Str( gDUT.dblElapsed, "#.00" ) + " Secs."
                }
                else
                {
                    // Still discharging
                }
            }
            else if( gDUT.dblCapVoltage < ( cDISCHARGE_DONE_VOLTAGE - 1.0 ) )
            {
                vTransToEvaluate();
            }
                

            //  From Xojo Implementation:
            // 
            //  If( gDUT.dblCapVoltage >= App.cDISCHARGE_START_VOLTAGE ) Then
            //      MainWindow.gSM.eCounter = eTIMING_STATE.eSTATE_READY
            //      gDUT.dblElapsed = 0.0
            //         
            //       
            //  ElseIf( ( gDUT.dblCapVoltage < App.cDISCHARGE_START_VOLTAGE ) And( gDUT.dblCapVoltage > ( App.cDISCHARGE_START_VOLTAGE - 0.2 ) ) ) Then   // Give it a little range in case we didn't get a good reading
            //      gDUT.intTenVoltTime = Ticks
            //      eCounter = eTIMING_STATE.eSTATE_COUNTING     // Start measuring critical timing
            //      MainWindow.vDrawVertTimeMarker( MainWindow.ScopeTrace.graphics, 0.1 ) // Update GUI with starting marker line
            //       
            //       
            //  ElseIf( eTIMING_STATE.eSTATE_COUNTING = eCounter ) Then
            //  
            //     // We are in the critical measurement band
            //      If( gDUT.dblCapVoltage < App.cDISCHARGE_DONE_VOLTAGE ) Then
            //          gDUT.dblElapsed = ( ( Ticks - gDUT.intTenVoltTime ) / 60 )
            //          eCounter = eTIMING_STATE.eSTATE_STOPPED // Stop measuring
            //          // update GUI scope display
            //          MainWindow.vDrawVertTimeMarker( MainWindow.ScopeTrace.graphics, gDUT.dblElapsed )
            //          MainWindow.txtMeasuredTime.Text = Str( gDUT.dblElapsed, "#.00" ) + " Secs."
            //      Else
            //          // Still discharging
            //      End If
            //                   
            //  ElseIf( gDUT.dblCapVoltage < ( App.cDISCHARGE_DONE_VOLTAGE - 1.0 ) ) Then
            //      vTransToEvaluate
            //  End If

            return;
        }


        private void vStFn_EvaluateResult()
        {
            if( StateTime > cTIMEOUT_TIME )
            {
                vTransToFail( eFAIL_TYPE.eFAIL_TIMEOUT_DISCHARGING );
            }
            else
            {
                if( gDUT.dblElapsed < cMIN_PASSING_TIME )
                {
                    // Discharged too fast. Capacitance too small
                    vTransToFail( eFAIL_TYPE.eFAIL_DISCHARGE_TOO_FAST );
                }
                else if( gDUT.dblElapsed > cMAX_PASSING_TIME )
                {
                    // Discharged too slow.  Capacitance too large
                    vTransToFail( eFAIL_TYPE.eFAIL_DISCHARGE_TOO_SLOW );
                }
                else
                {
                    // Value is good
                    vTransToPostResults();
                }
            }

            //  From Xojo Implementation:
            // 
            //  If( StateTime > App.cTIMEOUT_TIME ) Then
            //      vTransToFail( eFAIL_TYPE.eFAIL_TIMEOUT_DISCHARGING )
            //  Else
            //
            //      If( gDUT.dblElapsed < App.cMIN_PASSING_TIME ) Then
            //          // Discharged too fast.  Capacitance too small
            //          vTransToFail( eFAIL_TYPE.eFAIL_DISCHARGE_TOO_FAST )
            //    
            //      Elseif( gDUT.dblElapsed > App.cMAX_PASSING_TIME ) Then
            //          // Discharged too slow.  Capacitance too large
            //          vTransToFail( eFAIL_TYPE.eFAIL_DISCHARGE_TOO_SLOW )
            //    
            //      Else
            //          // Value is good
            //          vTransToPostResults
            //      End If
            //
            //  End If

            return;
        }


        private void vStFn_PostResults()
        {
            vFreeDUTObject();
            vTransToReady();
            // MsgBox( "Done" )


            //  From Xojo Implementation:
            // 
            //  vFreeDUTObject
            //  vTransToReady
            //  MsgBox( "DONE" )

            return;
        }


        private void vStFn_Failed()
        {
            gTester.vDischargeCap();
            vTransToReady();

            //  From Xojo Implementation:
            // 
            //  TesterCtrl.vDischargeCap
            //  vTransToReady

            return;
        }
        #endregion

        #region Transition Functions

        void vTransStartDischarge()
        {
            gTester.vStopCharging();            // Stop charging
            gTester.vDischargeCap();            // Start discharging
            eCounter = eTIMING_STATE_TYPE.eSTATE_READY;                  // Get the measurement counter ready to count 
            eCurrentState = eTESTING_STATE_TYPE.eTST_STATE_DISCHARGING;  // Start discharging

            return;
        }

        void vTransToEvaluate()
        {
            gTester.vDrainCap();
            eCurrentState = eTESTING_STATE_TYPE.eTST_STATE_EVALUATE;

            return;
        }

        void vTransToFail( eFAIL_TYPE eFailure )
        {
            gTester.vStopCharging();
            gTester.vStopDischarging();
            gstrFailText = strFailNames[(int)eFailure];
            // MJS MsgBox( gstrFailText );
            eCurrentState = eTESTING_STATE_TYPE.eTST_STATE_FAILED;

            return;
        }

        public void vTransToNoComm()
        {
            eCurrentState = eTESTING_STATE_TYPE.eTST_STATE_NO_COMM;
            // MainWindow.vResetGui
            if( null != gDUT )
            {
                vFreeDUTObject(); // Destroy and free memory used by DUT object
            }

            return;
        }

        void vTransToPostResults()
        {
            gTester.vStopDischarging();
            gTester.vStopCharging();
            // MJS MainWindow.vLogPassResult( "00000A170000000", "Now", gDUT.dblElapsed );
            eCurrentState = eTESTING_STATE_TYPE.eTST_STATE_POST_RESULTS;
            // MJS MsgBox( "Passed!" );

            return;
        }

        public void vTransToReady()
        {
            // We got here by scanning a valid barcode
            // Fake it for now
            if( null == gDUT )
            {
                gDUT = new DUTClass( "00000Z32118123456" ); // First, create the DUT object with the PCBA barcode number
                gDUT.dblElapsed = 0.0;
            }

            gTester.vDrainCap();
            // MJS MainWindow.vResetGui();

            eCurrentState = eTESTING_STATE_TYPE.eTST_STATE_READY;

            return;
        }

        public void vTransToStart()
        {
            // MJS MainWindow.vResetGui();
            gTester.vChargeCap();
            eCurrentState = eTESTING_STATE_TYPE.eTST_STATE_CHARGE;

            return;
        }

        #endregion

    } // End "class StateMachineClass"

} // End "namespace _24499_OCTP_Uninterruptable_Power_Supply_ICT_PC_Tool"


