// IDS.AppInfo.cs
// Version 1.0
//
// History
// 1.0 -- first release
//
//
// Class is useful for reading properties of the application, such as:
// - application title
// - version
// - build date
// - copyright
// - company name
// - default icon

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;

namespace IDS
{
    // get version information, etc, out of the app
    static class AppInfo
    {
        class Win32
        {
            public const int LR_LOADFROMFILE = 0x0010;

            [DllImport("user32.dll")]
            public static extern uint PrivateExtractIcons(string lpszFile, int nIconIndex, int cxIcon, int cyIcon, out IntPtr phicon, out uint piconid, uint nIcons, uint flag);

            [DllImport("user32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool DestroyIcon(IntPtr hIcon);
        }

        static FileVersionInfo oFileVersionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

        static Version mVersion = Assembly.GetExecutingAssembly().GetName().Version;
        static public Version Version { get { return mVersion; } }

        static DateTime mBuildDate;
        static public DateTime BuildDate { get { return mBuildDate; } }

        static public String CompanyName { get { return oFileVersionInfo.CompanyName; } }
        static public String Title { get { return oFileVersionInfo.FileDescription; } }
        static public String ProductName { get { return oFileVersionInfo.ProductName; } }
        static public String Copyright { get { return oFileVersionInfo.LegalCopyright; } }
        static public String Trademarks { get { return oFileVersionInfo.LegalTrademarks; } }

        static AppInfo()
        {
            // create the build date
            mBuildDate = new DateTime(2000, 1, 1); // Build dates start from 01/01/2000
            mBuildDate = mBuildDate.AddDays(Version.Build); // Add the number of days (build)
            mBuildDate = mBuildDate.AddSeconds(Version.Revision * 2); // Add the number of seconds since midnight (revision) multiplied by 2

            // build date should account for daylight saving time
            // this code doesn't work right on some platforms
            //            if (TimeZone.IsDaylightSavingTime(mBuildDate, TimeZone.CurrentTimeZone.GetDaylightChanges(mBuildDate.Year)))
            //                mBuildDate = mBuildDate.AddHours(1); // If we're currently in daylight saving time add an extra hour
        }

        static public Icon GetIcon(int x, int y)
        {
            // set the icon to the default application icon
            IntPtr hicon;
            uint id;
            uint count = Win32.PrivateExtractIcons(
                Assembly.GetExecutingAssembly().Location,
                0,
                x,
                y,
                out hicon,
                out id,
                1,
                Win32.LR_LOADFROMFILE);

            if (count < 1)
                return SystemIcons.Application;

            // create the icon object
            // NOTE: must clone the icon before destroying hicon
            Icon icon = (Icon)Icon.FromHandle(hicon).Clone();
            Win32.DestroyIcon(hicon);
            return icon;
        }
    }
}
