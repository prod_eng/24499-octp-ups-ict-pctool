namespace IDS
{
    partial class AboutBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PictureBox = new System.Windows.Forms.PictureBox();
            this.OKButton = new System.Windows.Forms.Button();
            this.AppInfo = new System.Windows.Forms.Label();
            this.AppTitle = new System.Windows.Forms.Label();
            this.AppVersion = new System.Windows.Forms.Label();
            this.Legal = new System.Windows.Forms.Label();
            this.SystemInfoButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBox
            // 
            this.PictureBox.Location = new System.Drawing.Point(12, 12);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(64, 64);
            this.PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox.TabIndex = 0;
            this.PictureBox.TabStop = false;
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.OKButton.Location = new System.Drawing.Point(227, 165);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 1;
            this.OKButton.TabStop = false;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            // 
            // AppInfo
            // 
            this.AppInfo.AutoSize = true;
            this.AppInfo.BackColor = System.Drawing.Color.Transparent;
            this.AppInfo.Location = new System.Drawing.Point(12, 92);
            this.AppInfo.Name = "AppInfo";
            this.AppInfo.Size = new System.Drawing.Size(44, 13);
            this.AppInfo.TabIndex = 3;
            this.AppInfo.Text = "AppInfo";
            // 
            // AppTitle
            // 
            this.AppTitle.AutoSize = true;
            this.AppTitle.BackColor = System.Drawing.Color.Transparent;
            this.AppTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppTitle.Location = new System.Drawing.Point(82, 12);
            this.AppTitle.Name = "AppTitle";
            this.AppTitle.Size = new System.Drawing.Size(54, 13);
            this.AppTitle.TabIndex = 4;
            this.AppTitle.Text = "AppTitle";
            // 
            // AppVersion
            // 
            this.AppVersion.AutoSize = true;
            this.AppVersion.BackColor = System.Drawing.Color.Transparent;
            this.AppVersion.Location = new System.Drawing.Point(82, 25);
            this.AppVersion.Name = "AppVersion";
            this.AppVersion.Size = new System.Drawing.Size(61, 13);
            this.AppVersion.TabIndex = 5;
            this.AppVersion.Text = "AppVersion";
            // 
            // Legal
            // 
            this.Legal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Legal.BackColor = System.Drawing.Color.Transparent;
            this.Legal.Location = new System.Drawing.Point(12, 133);
            this.Legal.Name = "Legal";
            this.Legal.Size = new System.Drawing.Size(209, 55);
            this.Legal.TabIndex = 6;
            this.Legal.Text = "All rights reserved.  Unauthorized reproduction or distribution of this program i" +
                "s prohibited, and will be prosecuted to the maximum extend possible under law.";
            // 
            // SystemInfoButton
            // 
            this.SystemInfoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SystemInfoButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SystemInfoButton.Location = new System.Drawing.Point(227, 133);
            this.SystemInfoButton.Name = "SystemInfoButton";
            this.SystemInfoButton.Size = new System.Drawing.Size(75, 23);
            this.SystemInfoButton.TabIndex = 7;
            this.SystemInfoButton.TabStop = false;
            this.SystemInfoButton.Text = "System Info";
            this.SystemInfoButton.UseVisualStyleBackColor = true;
            this.SystemInfoButton.Click += new System.EventHandler(this.SystemInfoButton_Click);
            // 
            // AboutBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundBrush.Angle = 0F;
            this.BackgroundBrush.Color1 = System.Drawing.Color.Silver;
            this.BackgroundBrush.Color2 = System.Drawing.Color.Black;
            this.BackgroundBrush.Mode = IDS.GradientBrush.GradientMode.ForwardDiagonal;
            this.BackgroundBrush.Rectangle = new System.Drawing.Rectangle(0, 0, 314, 200);
            this.ClientSize = new System.Drawing.Size(314, 200);
            this.Controls.Add(this.SystemInfoButton);
            this.Controls.Add(this.Legal);
            this.Controls.Add(this.AppVersion);
            this.Controls.Add(this.AppTitle);
            this.Controls.Add(this.AppInfo);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.PictureBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutBox";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About";
            this.VisibleChanged += new System.EventHandler(this.AboutBox_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Label AppInfo;
        private System.Windows.Forms.Label AppTitle;
        private System.Windows.Forms.Label AppVersion;
        private System.Windows.Forms.Label Legal;
        private System.Windows.Forms.Button SystemInfoButton;
    }
}